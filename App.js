import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import Age from './app/components/Age';
import Lista from './app/components/Lista';
export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Lista />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  text: {
    alignItems: 'center',
    padding: 10,
  },
  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },
});
