import React from 'react';
import {
  View,
  Text,
  FlatList,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Image,
} from 'react-native';
import {List, ListItem} from 'react-native-elements';
const DATA = [
  {
    id: 1,
    titulo: 'Primer elemento',
    descripcion:
      'Nave espacial Nasa, modelo escala. Apolo 369 - similar nave spacex Centurion',
    img: 'https://image.freepik.com/vector-gratis/composicion-isometrica-transbordador-espacial_1284-20656.jpg',
  },
  {
    id: 2,
    titulo: 'Segundo elemento',
    descripcion:
      'Astronauta sobre JUpiter, tantos deseos por cumplirse. Traje 2031 colonizacion Luna',
    img: 'https://image.freepik.com/vector-gratis/astronauta-sentado-planeta-agitando-mano-dibujos-animados-vector-icono-ilustracion-concepto-icono-tecnologia-ciencia-aislado-vector-premium-estilo-dibujos-animados-plana_138676-3503.jpg',
  },
  {
    id: 3,
    titulo: 'Tercer elemento',
    descripcion:
      'Astronauta perdio su mision en Marte, no se encontraron los recursos para producri amoniaco e hidrogeno',
    img: 'https://image.freepik.com/vector-gratis/astronauta-esperando-marte-ilustracion-diseno_72076-299.jpg',
  },
  {
    id: 4,
    titulo: 'Cuarto elemento',
    descripcion:
      'Colonizando Marte, viendolo desde el mismo espacio a una velocidad no afectada por el tiempo foton spacex',
    img: 'https://image.freepik.com/vector-gratis/vuelo-marte-ilustracion-estilo-rerto_185417-31.jpg',
  },
  {
    id: 5,
    titulo: 'Sexto elemento',
    descripcion:
      'Medalla Astronauta, privilegios grupo mesa redonda y rey arturo, mision HG-2531-0025',
    img: 'https://image.freepik.com/vector-gratis/astronauta-reza-galaxia_332261-28.jpg',
  },
  {
    id: 6,
    titulo: 'Primer elemento',
    descripcion:
      'Nave espacial Nasa, modelo escala. Apolo 369 - similar nave spacex Centurion',
    img: 'https://image.freepik.com/vector-gratis/composicion-isometrica-transbordador-espacial_1284-20656.jpg',
  },
];

const Item = ({title, img, descripcion}) => (
  <View style={styles.root}>
    <Image
      source={{
        uri: img,
      }}
      style={styles.image}
    />
    <View style={styles.rigthContainer}>
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.descripcion} numberOfLines={4}>
        {descripcion}
      </Text>
      <Text style={styles.price}>
        Eliminar
        <Text style={styles.oldPrice}> S/.16.22</Text>
      </Text>
    </View>
  </View>
);

const Lista = () => {
  const renderItem = ({item}) => (
    <Item title={item.titulo} img={item.img} descripcion={item.descripcion} />
  );

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={DATA}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  page: {},
  root: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#3498DB',
    borderRadius: 10,
    backgroundColor: '#fff',
    marginBottom: 4,
  },
  image: {
    flex: 2,
    height: 150,
    resizeMode: 'contain',
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  descripcion: {
    fontSize: 14,
  },
  price: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#3498DB',
  },
  oldPrice: {
    fontSize: 12,
    fontWeight: 'normal',
    textDecorationLine: 'line-through',
  },
  rigthContainer: {
    padding: 10,
    backgroundColor: '#fff',
    flex: 3,
  },
  ratingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  star: {
    margin: 3,
  },
});
export default Lista;
