import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

const Age = () => {
  const [Age, setAge] = useState(0);
  const [Rpta, setRpta] = useState('Ingrese edad valida');

  function AgeUp() {
    let edad = Age;
    if (edad >= 18) {
      setRpta('Es mayor de edad');
    } else {
      setRpta('No es mayor de edad');
    }
  }
  onPress = () => {
    AgeUp();
  };
  return (
    <View>
      <Text style={styles.title}>Ingrese su edad por favor!</Text>
      <TextInput
        keyboardType="numeric"
        style={styles.inputText}
        onChangeText={setAge}
      />
      <TouchableOpacity style={styles.button} onPress={onPress}>
        <Text style={styles.titleButton}> Verificar</Text>
      </TouchableOpacity>
      <Text style={styles.rptaText}>{Rpta}</Text>
    </View>
  );
};
const styles = StyleSheet.create({
  button: {
    color: '#48C9B0',
    fontSize: 16,
    padding: 15,
    backgroundColor: '#48C9B0',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    marginVertical: 5,
    height: 60,
  },
  rptaText: {
    marginTop: 14,
    color: 'red',
    fontSize: 25,
    textAlign: 'center',
  },
  title: {
    fontSize: 22,
    marginBottom: 20,
    fontWeight: 'bold',
    color: '#1F2937',
    textAlign: 'center',
  },
  inputText: {
    backgroundColor: '#E5E7EB',
    padding: 15,
    paddingLeft: 55,
    paddingRight: 55,
    borderRadius: 5,
    fontSize: 16,
    height: 60,
    marginVertical: 3,
    marginBottom: 10,
    color: '#1F2937',
  },
  titleButton: {
    fontSize: 18,
    color: '#FFFD',
    fontWeight: 'bold',
  },
});
export default Age;
